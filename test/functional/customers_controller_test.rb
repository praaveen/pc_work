require 'test_helper'

class CustomersControllerTest < ActionController::TestCase
  setup do
    @customer = customers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:customers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create customer" do
    assert_difference('Customer.count') do
      post :create, customer: { brief_details: @customer.brief_details, company_name: @customer.company_name, contact_number: @customer.contact_number, contact_person_name: @customer.contact_person_name, crawl_frequency: @customer.crawl_frequency, data_format: @customer.data_format, email_id: @customer.email_id, project_name: @customer.project_name, seed_url: @customer.seed_url }
    end

    assert_redirected_to customer_path(assigns(:customer))
  end

  test "should show customer" do
    get :show, id: @customer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @customer
    assert_response :success
  end

  test "should update customer" do
    put :update, id: @customer, customer: { brief_details: @customer.brief_details, company_name: @customer.company_name, contact_number: @customer.contact_number, contact_person_name: @customer.contact_person_name, crawl_frequency: @customer.crawl_frequency, data_format: @customer.data_format, email_id: @customer.email_id, project_name: @customer.project_name, seed_url: @customer.seed_url }
    assert_redirected_to customer_path(assigns(:customer))
  end

  test "should destroy customer" do
    assert_difference('Customer.count', -1) do
      delete :destroy, id: @customer
    end

    assert_redirected_to customers_path
  end
end
