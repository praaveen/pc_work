class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :contact_person_name
      t.string :company_name
      t.string :email_id
      t.integer :contact_number
      t.string :project_name
      t.text :brief_details
      t.string :seed_url
      t.string :data_format
      t.integer :crawl_frequency

      t.timestamps
    end
  end
end
