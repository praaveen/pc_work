class AddDataSchemaToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :data_schema, :text
  end
end
