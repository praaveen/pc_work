class Customer < ActiveRecord::Base
  before_save :modifi_data_format

  attr_accessible :pic,:data_schema, :attach, :brief_details, :company_name, :contact_number, :contact_person_name, :crawl_frequency, :data_format, :email_id, :project_name, :seed_url
  has_attached_file :pic, :styles =>{ :medium => "300x300>", :thumb => "100x100>" }
  has_attached_file :attach


  validates :brief_details, length: { minimum: 4, maximum: 500 }
  validates :company_name, length: { minimum: 4, maximum:20 }
  validates :contact_number,numericality: true
  validates :contact_person_name, length:{ minimum: 4, maximum:20 }
  validates :crawl_frequency,numericality: true
  validates :data_format, presence: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email_id, presence: true,
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: { case_sensitive: false }
  validates :project_name, length: { minimum: 4, maximum:20 }
  validates :seed_url, presence: true





  def modifi_data_format
    a = self.data_format.to_a
    self.data_format = []
    a.each do |i|
      self.data_format << i if ! i.empty?
    end
  end
end
